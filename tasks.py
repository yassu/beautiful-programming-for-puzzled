import term
from invoke import task


def print_title(title: str) -> None:
    term.writeLine('running ' + title, term.green)


def _run_commands(ctx, commands):
    for command in commands:
        print(command)
        ctx.run(command)


@task(aliases=['f'])
def format(ctx):
    print_title('yapf')
    ctx.run('yapf -i **/*.py')
    print_title('isort')
    ctx.run('isort **/*.py')


@task
def complexity(ctx):
    print_title('radon')
    ctx.run('radon cc -s --total-average  beautiful_programming_for_puzzled/')


@task(aliases=['t'])
def test(ctx):
    print_title('pytest')
    ctx.run('pytest -vv --cov=beautiful_programming_for_puzzled tests')
    print_title('pytest with cov-report')
    ctx.run(
        'pytest --cov=beautiful_programming_for_puzzled tests --cov-report=html > /dev/null 2>&1')
    print_title('mypy')
    ctx.run('mypy .')
    print_title('flake8')
    ctx.run('flake8 *.py beautiful_programming_for_puzzled')
    print_title('isort')
    ctx.run('isort --check-only beautiful_programming_for_puzzled/ tests/')
    complexity(ctx)
