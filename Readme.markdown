beautiful-programming-for-puzzled
================================================================================


[![image](https://gitlab.com/yassu/beautiful-programming-for-puzzled/badges/master/pipeline.svg)](https://gitlab.com/yassu/beautiful-programming-for-puzzled/-/pipelines/latest)
[![coverage report](https://gitlab.com/yassu/beautiful-programming-for-puzzled/badges/master/coverage.svg)](https://gitlab.com/yassu/beautiful-programming-for-puzzled/-/pipelines/latest)


Abstract
--------------------------------------------------------------------------------


This project provides more beautiful code for [Programming for the Puzzled](https://www.amazon.co.jp/dp/0262534304).

While I read this book, I will write source code.


[![Programming for the Puzzled](https://m.media-amazon.com/images/I/516FEMjZ5cL._SX260_.jpg)](https://www.amazon.co.jp/dp/0262534304)



Source Code
--------------------------------------------------------------------------------


| Title                                                | Source Code                                  | Test Code                      |
| ---                                                  | ---                                          | ---                            |
| Ch01. You Will All Conform                           | [get_order](https://bit.ly/2N5H5pB)          | [test](https://bit.ly/2N10qIC) |
| Ch02. The Best Time to Party                         | [compute_best_time](https://bit.ly/2O7Y8b7)  | [test](https://bit.ly/3ry9XpH) |
| Ch03. You Can Read Minds(with a Little  Calibration) | [assistant_main](https://bit.ly/3cNNpx2)     | [test](https://bit.ly/36NkghD) |
|                                                      | [magician_main](https://bit.ly/3cMmrpj)      | [test](https://bit.ly/3cNNG32) |
| Ch04. Keep Those Queens Apart                        | [queens](https://bit.ly/3cKyXWn)             | [test](https://bit.ly/36JoneD) |
| Ch05. Please Do Break the Crystal                    | [get_dropped_counts](https://bit.ly/3tyGVYU) | [test](https://bit.ly/3pZDpUY) |
| Ch06. Find That Fake                                 | [search_fake_coin](https://bit.ly/36KtY4n)   | [test](https://bit.ly/3p3gVBu) |
| Ch07. Hip to Be a Square Root                        | [bin_search](https://bit.ly/3b39IfN)         | [test](https://bit.ly/2NenKCH) |
|                                                      | [my_sqrt](https://bit.ly/3cW3r83)            | [test](https://bit.ly/3jBRPbx) |
| Ch08. Guess Who Isn't Coming to Dinner               | [invites](https://bit.ly/3p4D3uU)            | [test](https://bit.ly/3a58EJ0) |
| Ch09. America's Got Talent                           | [compute_talents](https://bit.ly/3tHWrRZ)    | [test](https://bit.ly/3rCtCVg) |
| Ch10. A Profusion of Queens                          | [r_gcd](https://bit.ly/36YXQdj)              | [test](https://bit.ly/3cTXoRt) |
|                                                      | [r_fib](https://bit.ly/3tGAfrD)              | [test](https://bit.ly/3aKFX3a) |
| Ch11. Tile That Courtyard, Please                    | [fill_tile](https://bit.ly/3b3bRbl)          | [test](https://bit.ly/3q5PGaH) |


LICENSE
-------


[MIT](https://gitlab.com/yassu/beautiful-programming-for-puzzled/blob/master/LICENSE)

