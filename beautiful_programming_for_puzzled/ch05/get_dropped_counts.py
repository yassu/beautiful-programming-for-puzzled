from typing import List


def base_r_to_10(ns: List[int], r: int) -> int:
    len_ = len(ns)

    number = 0
    for i in range(len_ - 1):
        number = (number + ns[i]) * r
    number += ns[len_ - 1]
    return number


def get_dropped_counts(max_floors: int, number_of_balls: int, max_floor_not_breaked) -> int:
    """
    max_floor: number of floors of the building
    max_floor_not_breaked: Maximum number of floors where the ball does not break
    """
    r = 1
    while r**number_of_balls <= max_floors:
        r += 1

    number_of_drops = 0
    now_floor = [0] * number_of_balls

    for i in range(number_of_balls):
        for j in range(r - 1):
            now_floor[i] += 1
            floor = base_r_to_10(now_floor, r)
            if floor > max_floors:
                break

            number_of_drops += 1
            if floor > max_floor_not_breaked:
                now_floor[i] -= 1
                break

    return number_of_drops
