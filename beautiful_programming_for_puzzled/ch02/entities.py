from enum import Enum


class TimePartitionType(str, Enum):
    START = 'start'
    END = 'end'
