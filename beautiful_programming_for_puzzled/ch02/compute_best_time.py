from typing import List, Tuple

from beautiful_programming_for_puzzled.ch02.entities import TimePartitionType


def _compute_best_time(times: List[Tuple[float, TimePartitionType]]) -> Tuple[int, float]:
    number = 0
    max_number = 0
    time = 0.0

    for t in times:
        if t[1] == TimePartitionType.START:
            number += 1
        elif t[1] == TimePartitionType.END:
            number -= 1

        if number > max_number:
            max_number = number
            time = t[0]

    return max_number, time


def compute_best_time(schedule: List[Tuple[float, float]]) -> Tuple[int, float]:
    time_partitions = []
    for c in schedule:
        time_partitions.append((c[0], TimePartitionType.START))
        time_partitions.append((c[1], TimePartitionType.END))

    time_partitions.sort(key=lambda x: x[0])

    return _compute_best_time(time_partitions)
