from collections import Counter
from typing import Generator, List, Tuple

from beautiful_programming_for_puzzled.ch03.entities import Card, Suit


def get_hidden_and_first_card(cards: List[Card]) -> Tuple[Card, Card]:
    assert len(cards) == 5

    suits = Counter([card.suit for card in cards])
    suit: Suit = None

    for s, num in suits.items():
        if num >= 2:
            suit = s
            break

    cards = list(filter(lambda c: c.suit == suit, cards))
    card1 = cards.pop()
    card2 = cards.pop()

    if (int(card1) - int(card2)) % 13 < (int(card2) - int(card1)) % 13:
        return card1, card2
    else:
        return card2, card1


def show_cards(cards: List[Card], *, hidden: Card, first: Card) -> Generator[Card, None, None]:
    cards = cards[:]
    assert len(cards) == 5
    assert first in cards
    assert hidden in cards

    yield first

    cards.remove(first)
    cards.remove(hidden)
    cards.sort(key=lambda c: (int(c), c.suit))

    dist = (int(hidden) - int(first)) % 13

    indexes = {
        1: (0, 1, 2),
        2: (0, 2, 1),
        3: (1, 0, 2),
        4: (1, 2, 0),
        5: (2, 0, 1),
        6: (2, 1, 0),
    }[dist]

    for i in indexes:
        yield cards[i]
