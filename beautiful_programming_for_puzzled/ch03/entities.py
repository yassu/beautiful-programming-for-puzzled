from __future__ import annotations

from dataclasses import dataclass
from enum import IntEnum, auto
from itertools import product
from typing import Generator


class Suit(IntEnum):
    CLOVER = auto()
    DIAMOND = auto()
    HEART = auto()
    SPADE = auto()


@dataclass(frozen=True)
class CardNumber:
    number: int

    def __post_init__(self):
        if not 1 <= self.number <= 13:
            raise ValueError(f'number {self.number} not in [1, 13].')


@dataclass(frozen=True)
class Card:
    suit: Suit
    number: CardNumber

    @classmethod
    def get_init_deck(cls) -> Generator[Card, None, None]:
        numbers = [CardNumber(n) for n in range(1, 13 + 1)]

        return (Card(suit=suit, number=number) for suit, number in product(Suit, numbers))

    def __int__(self) -> int:
        return self.number.number
