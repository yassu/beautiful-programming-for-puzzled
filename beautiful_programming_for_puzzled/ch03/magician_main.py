from typing import List

from beautiful_programming_for_puzzled.ch03.entities import Card, CardNumber


def guess_card(cards: List[Card]) -> Card:
    assert len(cards) == 4
    suit = cards[0].suit
    base_number = int(cards[0])

    cards = cards[:]
    cards = cards[1:]
    cards_tmp = cards[:]
    cards_tmp.sort(key=lambda c: (int(c), c.suit))

    scores = tuple(cards_tmp.index(c) for c in cards)
    dist = {
        (0, 1, 2): 1,
        (0, 2, 1): 2,
        (1, 0, 2): 3,
        (1, 2, 0): 4,
        (2, 0, 1): 5,
        (2, 1, 0): 6,
    }[scores]  # type: ignore

    number = (base_number + dist) % 13
    if number == 0:
        number = 13

    return Card(suit=suit, number=CardNumber(number))
