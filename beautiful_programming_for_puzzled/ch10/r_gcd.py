def r_gcd(a: int, b: int) -> int:
    if a % b == 0:
        return b
    else:
        return r_gcd(b, a % b)


if __name__ == '__main__':
    print(r_gcd(2002, 1344))
