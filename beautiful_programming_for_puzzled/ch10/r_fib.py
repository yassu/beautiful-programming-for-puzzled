def r_fib(n: int) -> int:
    if n in (0, 1):
        return n
    else:
        return r_fib(n - 2) + r_fib(n - 1)
