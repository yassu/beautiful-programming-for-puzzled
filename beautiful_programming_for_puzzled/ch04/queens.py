from copy import deepcopy
from typing import Generator, List


def convert_to_pretty_board(board: List[int]) -> List[List[str]]:
    n = len(board)
    mat = [['.' for _ in range(n)] for _ in range(n)]

    for i, val in enumerate(board):
        if val == -1:
            continue
        else:
            mat[val][i] = 'Q'

    return mat


def exists_conflicts(board: List[int], now_i: int):
    for i in range(now_i):
        if board[i] == board[now_i]:
            return True
        if now_i - i == abs(board[now_i] - board[i]):
            return True

    return False


def get_queens(n: int) -> Generator[List[int], None, None]:

    def _get_queens(board: List[int], k: int) -> Generator[List[int], None, None]:
        if k == n:
            yield deepcopy(board)
            return

        for val in range(n):
            board[k] = val
            if exists_conflicts(board, k):
                continue

            yield from _get_queens(board, k + 1)

    return _get_queens([-1] * n, 0)
