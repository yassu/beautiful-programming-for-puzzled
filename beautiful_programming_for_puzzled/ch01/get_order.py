from typing import Generator, List, Tuple

from beautiful_programming_for_puzzled.ch01.entities import CapDirection


def get_order(directions: List[CapDirection]) -> Generator[Tuple[int, int], None, None]:
    n = len(directions)

    if n in (0, 1):
        yield from []
        return

    change_directions = set(CapDirection)
    change_directions.remove(directions[0])
    change_direction = change_directions.pop()

    order_start_index = -1
    for i, direction in enumerate(directions):
        if direction == change_direction and order_start_index == -1:
            order_start_index = i
        elif direction != change_direction and order_start_index != -1:
            yield (order_start_index, i)
            order_start_index = -1

    if order_start_index != -1:
        yield (order_start_index, n)
