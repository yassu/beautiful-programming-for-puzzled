from enum import Enum


class CapDirection(str, Enum):
    RIGHT = 'R'
    LEFT = 'L'
