from abc import ABCMeta, abstractmethod
from typing import Any, List, TypeVar


class Comparable(metaclass=ABCMeta):

    @abstractmethod
    def __lt__(self, other: Any) -> bool:
        ...

    @abstractmethod
    def __gt__(self, other: Any) -> bool:
        ...


CT = TypeVar('CT', bound=Comparable)


def bin_search(xs: List[CT], target: CT) -> int:
    low, high = 0, len(xs) - 1
    while low <= high:
        mid = (low + high) // 2
        if xs[mid] < target:
            low = mid + 1
        elif xs[mid] > target:
            high = mid - 1
        else:
            return mid

    return -1
