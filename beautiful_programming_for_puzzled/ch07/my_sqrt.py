from typing import Tuple


def my_sqrt(x: float, *, eps: float = 1e-4) -> Tuple[float, int]:
    if x < 0:
        raise ValueError(f'{x} is a negative number.')

    counts = 0
    low = 0.0
    high = max(x, 1)
    res = (high + low) / 2.0
    while abs(res**2 - x) >= eps:
        if res**2 < x:
            low = res
        else:
            high = res
        res = (high + low) / 2.0
        counts += 1

    return res, counts


def main() -> None:
    print(my_sqrt(0.2))


if __name__ == '__main__':
    main()
