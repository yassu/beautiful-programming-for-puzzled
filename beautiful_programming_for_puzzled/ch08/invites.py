from dataclasses import dataclass
from itertools import combinations
from typing import Generator, List, Set, Tuple


@dataclass(frozen=True)
class Person:
    id_: str


@dataclass(frozen=True)
class DislikePair:
    pair: frozenset

    def __post_init__(self):
        if len(self.pair) != 2:
            raise ValueError(f'len of {self.pair} should be 2.')

        for person in self.pair:
            if not isinstance(person, Person):
                raise ValueError(f'{person} is not a Person instance.')


def get_combinations(persons: List[Person]) -> Generator[Tuple[Person, ...], None, None]:
    n = len(persons)
    for r in reversed(range(n + 1)):
        yield from combinations(persons, r)


def is_ok_combination(persons: List[Person], dislike_pairs: Set[DislikePair]) -> bool:
    for dislike_pair_tmp in dislike_pairs:
        dislike_pair = dislike_pair_tmp.pair
        if dislike_pair.issubset(set(persons)):
            return False

    return True


def compute_invites_peoples(persons: List[Person],
                            dislike_pairs: Set[DislikePair]) -> Tuple[Person, ...]:
    for combination in get_combinations(persons):
        if is_ok_combination(list(combination), dislike_pairs):
            return tuple(combination)

    raise Exception('Not Reached.')
