from __future__ import annotations

from copy import deepcopy
from itertools import product
from typing import Final, List, Tuple

PRINTABLE: Final[List[str]] = list(
    '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
LOSS_NUMBER: Final[int] = -2
EMPTY_NUMBER: Final[int] = -1


def conv2str(n: int) -> str:
    if n == LOSS_NUMBER:
        return '!'
    elif n == EMPTY_NUMBER:
        return ' '
    else:
        return PRINTABLE[n]


class Board(list):

    def _fill(self, missing_pos: Tuple[int, int], printable: List[str]) -> Board:
        len_ = len(self)
        miss = PRINTABLE.index(printable[0])
        del printable[0]
        board = deepcopy(self)
        quadrant = get_quadrants(len_, missing_pos[0], missing_pos[1])

        if len_ == 2:
            poses = ((0, 0), (0, 1), (1, 0), (1, 1))
            for pos in poses:
                if pos == missing_pos:
                    continue
                y, x = pos
                board[y][x] = miss
            return board

        m = len_ // 2
        space_pos = {1: (m, m), 2: (m - 1, m), 3: (m - 1, m - 1), 4: (m, m - 1)}[quadrant]
        filled_poses = {(m, m), (m - 1, m), (m - 1, m - 1), (m, m - 1)}
        filled_poses.discard(space_pos)

        for pos in filled_poses:
            y, x = pos
            board[y][x] = miss

        for target_quadrant in (1, 2, 3, 4):
            y_interval = {1: (m, len_), 2: (m, len_), 3: (0, m), 4: (0, m)}[target_quadrant]
            x_interval = {1: (m, len_), 2: (0, m), 3: (0, m), 4: (m, len_)}[target_quadrant]
            small_board = Board(
                [
                    [self[y][x]
                     for y in range(y_interval[0], y_interval[1])]
                    for x in range(x_interval[0], x_interval[1])
                ])
            small_missing_pos = {
                1: (0, 0),
                2: (0, m - 1),
                3: (m - 1, m - 1),
                4: (m - 1, 0)
            }[target_quadrant]
            small_board = deepcopy(small_board._fill(small_missing_pos, printable))

            for y, x in product(range(y_interval[0], y_interval[1]), range(x_interval[0],
                                                                           x_interval[1])):
                num = small_board[y - y_interval[0]][x - x_interval[0]]
                if board[y][x] == EMPTY_NUMBER:
                    board[y][x] = num

        return board

    def fill(self) -> Board:
        len_ = len(self)
        missing_pos = (-1, -1)
        for y in range(len_):
            for x in range(len_):
                if self[y][x] == LOSS_NUMBER:
                    missing_pos = (y, x)

        return self._fill(missing_pos, PRINTABLE[:])

    @property
    def humanly(self) -> List[List[str]]:
        len_ = len(self)
        return [[conv2str(self[x][y]) for y in range(len_)] for x in range(len_)]

    @property
    def fancy_repr(self) -> str:
        len_ = len(self)
        h = self.humanly
        print([''.join([h[x][y] for y in range(len_)]) for x in range(len_)])
        s = '\n'.join([''.join([h[x][y] for y in range(len_)]) for x in range(len_)])
        return s


def get_quadrants(size: int, x: int, y: int):
    partition = size // 2

    if x >= partition and y >= partition:
        return 1
    elif y >= partition:
        return 2
    elif x < partition:
        return 3
    else:
        return 4
