from dataclasses import dataclass
from typing import List, Optional, Set


@dataclass(frozen=True)
class Person:
    id_: str
    talents: Set[int]


def _compute_persons(
        persons: List[Person], all_persons: List[Person],
        uncovered_talents: Set[int]) -> Optional[List[Person]]:
    if uncovered_talents == set():
        return persons

    persons = persons[:]
    all_persons = all_persons[:]
    target_talent = uncovered_talents.pop()
    target_persons = (p for p in all_persons if target_talent in p.talents)

    if target_persons == set():
        return None

    min_persons = all_persons
    min_len = len(all_persons)
    for target_person in target_persons:
        uncovered_talents2 = uncovered_talents.difference(target_person.talents)
        persons2 = _compute_persons(persons + [target_person], all_persons, uncovered_talents2)
        len_ = len(persons2)
        if len_ < min_len:
            min_persons, min_len = persons2, len_

    return min_persons


def compute_persons(persons: List[Person]) -> List[Person]:
    number_of_talents = -1
    for p in persons:
        number_of_talents = max(number_of_talents, max(p.talents) + 1)

    return _compute_persons([], persons, set(range(number_of_talents)))
