from enum import IntEnum
from typing import List, Tuple

Coin = int


class CompareResult(IntEnum):
    LEFT = -1
    EQUAL = 0
    RIGHT = 1


def compare_group(group1: List[Coin], group2: List[Coin]) -> CompareResult:
    if sum(group1) > sum(group2):
        return CompareResult.LEFT
    elif sum(group2) > sum(group1):
        return CompareResult.RIGHT
    else:
        return CompareResult.EQUAL


def split_coins(coins: List[Coin]) -> Tuple[List[Coin], List[Coin], List[Coin]]:
    len_ = len(coins)
    return coins[:len_ // 3], coins[len_ // 3:len_ // 3 * 2], coins[len_ // 3 * 2:]


def find_fake_group(group1: List[Coin], group2: List[Coin], group3: List[Coin]) -> List[Coin]:
    compare1_and_2 = compare_group(group1, group2)

    if compare1_and_2 == CompareResult.LEFT:
        return group1
    elif compare1_and_2 == CompareResult.EQUAL:
        return group3
    else:
        return group2


def run_coin_comp(coins: List[Coin]) -> Tuple[Coin, int]:
    """
    returns the fake coin and number of weighings
    """
    counts = 0
    coins = coins[:]

    while len(coins) > 1:
        group1, group2, group3 = split_coins(coins)
        coins = find_fake_group(group1, group2, group3)
        counts += 1

    fake = coins[0]

    return fake, counts
