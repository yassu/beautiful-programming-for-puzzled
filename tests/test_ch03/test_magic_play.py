from random import shuffle

import pytest

from beautiful_programming_for_puzzled.ch03.assistant_main import (
    get_hidden_and_first_card,
    show_cards,
)
from beautiful_programming_for_puzzled.ch03.entities import Card
from beautiful_programming_for_puzzled.ch03.magician_main import guess_card


@pytest.mark.repeat(100)
def test_play():
    deck = list(Card.get_init_deck())
    shuffle(deck)
    cards = deck[:5]
    original_cards = set(cards)

    hidden, first = get_hidden_and_first_card(cards)
    showns = list(show_cards(cards, hidden=hidden, first=first))

    result = guess_card(showns)
    assert result == hidden, str(cards)

    showns.append(hidden)
    assert set(showns) == set(cards) == original_cards
