from beautiful_programming_for_puzzled.ch03.assistant_main import (
    get_hidden_and_first_card,
    show_cards,
)
from beautiful_programming_for_puzzled.ch03.entities import Card, CardNumber, Suit


def test_get_hidden_card():
    cards = [
        Card(suit=Suit.HEART, number=CardNumber(10)),
        Card(suit=Suit.DIAMOND, number=CardNumber(9)),
        Card(suit=Suit.HEART, number=CardNumber(3)),
        Card(suit=Suit.SPADE, number=CardNumber(12)),
        Card(suit=Suit.CLOVER, number=CardNumber(11)),
    ]

    hidden, first = get_hidden_and_first_card(cards)
    assert hidden == Card(suit=Suit.HEART, number=CardNumber(3))
    assert first == Card(suit=Suit.HEART, number=CardNumber(10))


def test_show_cards():
    cards = [
        Card(suit=Suit.HEART, number=CardNumber(10)),
        Card(suit=Suit.DIAMOND, number=CardNumber(9)),
        Card(suit=Suit.HEART, number=CardNumber(3)),
        Card(suit=Suit.SPADE, number=CardNumber(12)),
        Card(suit=Suit.CLOVER, number=CardNumber(11)),
    ]
    hidden, first = get_hidden_and_first_card(cards)
    result = list(show_cards(cards, hidden=hidden, first=first))
    assert len(cards) == 5

    cards.remove(hidden)
    assert result == [
        Card(suit=Suit.HEART, number=CardNumber(10)),
        Card(suit=Suit.SPADE, number=CardNumber(12)),
        Card(suit=Suit.CLOVER, number=CardNumber(11)),
        Card(suit=Suit.DIAMOND, number=CardNumber(9)),
    ]
