import pytest

from beautiful_programming_for_puzzled.ch03.entities import Card, CardNumber, Suit
from beautiful_programming_for_puzzled.ch03.magician_main import guess_card

CARDS1 = [
    Card(suit=Suit.HEART, number=CardNumber(10)),
    Card(suit=Suit.SPADE, number=CardNumber(12)),
    Card(suit=Suit.CLOVER, number=CardNumber(11)),
    Card(suit=Suit.DIAMOND, number=CardNumber(9)),
]

CARDS2 = [
    Card(suit=Suit.HEART, number=CardNumber(10)),
    Card(suit=Suit.SPADE, number=CardNumber(12)),
    Card(suit=Suit.CLOVER, number=CardNumber(11)),
    Card(suit=Suit.SPADE, number=CardNumber(9)),
]

CARDS3 = [
    Card(suit=Suit.HEART, number=CardNumber(10)),
    Card(suit=Suit.SPADE, number=CardNumber(12)),
    Card(suit=Suit.HEART, number=CardNumber(11)),
    Card(suit=Suit.SPADE, number=CardNumber(9)),
]

EXPECTED = Card(suit=Suit.HEART, number=CardNumber(3))


@pytest.mark.parametrize(
    'cards, expected', [(CARDS1, EXPECTED), (CARDS2, EXPECTED), (CARDS3, EXPECTED)])
def test_guess_card(cards, expected):
    assert len(cards) == 4
    assert guess_card(cards) == Card(suit=Suit.HEART, number=CardNumber(3))
