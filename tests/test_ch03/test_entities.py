import pytest

from beautiful_programming_for_puzzled.ch03.entities import Card, CardNumber, Suit


def test_Suit_order():
    assert Suit.CLOVER < Suit.DIAMOND < Suit.HEART < Suit.SPADE


@pytest.mark.parametrize('n', [1, 7, 13])
def test_CardNumber_init(n):
    CardNumber(n)


@pytest.mark.parametrize('n', [0, 14])
def test_CardNumber_init_in_illegal_case(n):
    with pytest.raises(ValueError, match=r'number .* not in'):
        CardNumber(n)


def test_Card_get_init_deck_length():
    deck = tuple(Card.get_init_deck())
    assert len(deck) == 13 * 4


def test_Card_get_init_deck_not_duplicate():
    deck = tuple(Card.get_init_deck())
    assert len(set(deck)) == len(deck)


def test_Card_int():
    card = Card(suit=Suit.DIAMOND, number=CardNumber(4))
    assert int(card) == 4
