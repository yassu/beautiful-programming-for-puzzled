import pytest

from beautiful_programming_for_puzzled.ch04.queens import (
    convert_to_pretty_board,
    exists_conflicts,
    get_queens,
)


def test_convert_to_pretty_board():
    assert convert_to_pretty_board([0, 6, 4, 7, 3, -1, -1, -1]) == [
        ['Q'] + ['.'] * 7,
        ['.'] * 8,
        ['.'] * 8,
        ['.'] * 4 + ['Q'] + ['.'] * 3,
        ['.'] * 2 + ['Q'] + ['.'] * 5,
        ['.'] * 8,
        ['.'] * 1 + ['Q'] + ['.'] * 6,
        ['.'] * 3 + ['Q'] + ['.'] * 4,
    ]


@pytest.mark.parametrize('board, expected', [([0, 6, 4, 7, -1], False), ([0, 6, 4, 4, -1], True)])
def test_exists_conflicts(board, expected):
    assert exists_conflicts(board, 3) is expected


@pytest.mark.parametrize('n, expected', [(2, 0), (3, 0), (8, 92)])
def test_get_queens_count(n, expected):
    queens = list(get_queens(n))
    assert len(queens) == expected


def test_get_queens_elem():
    queens = list(get_queens(8))
    assert [6, 4, 2, 0, 5, 7, 1, 3] in queens
