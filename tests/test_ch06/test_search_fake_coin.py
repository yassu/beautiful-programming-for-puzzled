from random import randint

import pytest

from beautiful_programming_for_puzzled.ch06.search_fake_coin import (
    CompareResult,
    compare_group,
    find_fake_group,
    run_coin_comp,
    split_coins,
)


@pytest.mark.parametrize(
    'group1, group2, expected', [
        ([10, 10, 10], [10, 10, 10], CompareResult.EQUAL),
        ([10, 11, 10], [10, 10, 10], CompareResult.LEFT),
        ([10, 10, 10], [10, 11, 10], CompareResult.RIGHT),
    ])
def test_compare_group(group1, group2, expected):
    res = compare_group(group1, group2)
    assert res == expected


def test_split_coins():
    coins = [10] * 9
    coins[6] = 11
    expected = [10, 10, 10], [10, 10, 10], [11, 10, 10]

    assert split_coins(coins) == expected


@pytest.mark.parametrize(
    'group1, group2, group3, expected', [
        (
            [10, 11, 10],
            [10] * 3,
            [10] * 3,
            [10, 11, 10],
        ),
        ([10] * 3, [11, 10, 10], [10] * 3, [11, 10, 10]),
        ([10] * 3, [10] * 3, [10, 10, 11], [10, 10, 11]),
    ])
def test_find_fake_group(group1, group2, group3, expected):
    assert find_fake_group(group1, group2, group3) == expected


@pytest.mark.repeat(100)
def test_run_coin_comp():
    number_of_coins = 3**3
    coins = [10] * (number_of_coins)
    i = randint(0, number_of_coins - 1)
    coins[i] = 11

    fake, counts = run_coin_comp(coins)

    assert fake == 11
    assert 0 < counts < number_of_coins
    assert coins == [10] * i + [11] + [10] * (number_of_coins - i - 1)
