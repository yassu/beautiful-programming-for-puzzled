from typing import Final, List, Tuple

import pytest

from beautiful_programming_for_puzzled.ch01.entities import CapDirection
from beautiful_programming_for_puzzled.ch01.get_order import get_order

EXAMPLE_ARGUMENT: Final[List[CapDirection]] = [
    CapDirection.LEFT,
    CapDirection.LEFT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.LEFT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.LEFT,
    CapDirection.LEFT,
    CapDirection.RIGHT,
    CapDirection.LEFT,
]
EXAMPLE_ARGUMENT2: Final[List[CapDirection]] = [
    CapDirection.LEFT,
    CapDirection.LEFT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.LEFT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.RIGHT,
    CapDirection.LEFT,
    CapDirection.LEFT,
    CapDirection.RIGHT,
]
EXAMPLE_EXPECTED = (2, 5), (6, 9), (11, 12)

EXAMPLE_ARGUMENT_EMPTY: Final[List[CapDirection]] = []
EXAMPLE_EXPECTED_EMPTY: Tuple[Tuple[int, int], ...] = tuple()

EXAMPLE_ARGUMENT_ONE: Final[List[CapDirection]] = [CapDirection.RIGHT]
EXAMPLE_EXPECTED_ONE: Tuple[Tuple[int, int], ...] = tuple()


@pytest.mark.parametrize(
    'arg, expected', [
        (EXAMPLE_ARGUMENT, EXAMPLE_EXPECTED),
        (EXAMPLE_ARGUMENT2, EXAMPLE_EXPECTED),
        (EXAMPLE_ARGUMENT_EMPTY, EXAMPLE_EXPECTED_EMPTY),
        (EXAMPLE_ARGUMENT_ONE, EXAMPLE_EXPECTED_ONE),
    ])
def test_get_order(arg, expected):
    orders = tuple(get_order(arg))
    assert orders == expected
