import pytest

from beautiful_programming_for_puzzled.ch09.compute_talents import Person, compute_persons

EXAMPLE_PERSONS = [
    Person('A', {3, 4, 6}),
    Person('B', {0, 1, 7}),
    Person('C', {1, 3, 5, 8}),
    Person('D', {2, 5, 8}),
    Person('E', {1, 2, 8}),
    Person('F', {6, 7, 8}),
    Person('G', {0, 2, 6})
]
RESULT_IDS = {'A', 'B', 'D'}


@pytest.mark.parametrize(
    'persons, expected', [
        ([], set()), ([Person('A', {0})], {'A'}),
        ([Person('A', {0}), Person('B', {1})], {'A', 'B'}), (EXAMPLE_PERSONS, RESULT_IDS)
    ])
def test_compute_persons1(persons, expected):
    res = compute_persons(persons)
    assert {p.id_ for p in res} == expected
