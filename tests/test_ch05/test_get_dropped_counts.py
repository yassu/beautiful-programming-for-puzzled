from beautiful_programming_for_puzzled.ch05.get_dropped_counts import (
    base_r_to_10,
    get_dropped_counts,
)


def test_base_r_to_10():
    assert base_r_to_10([3, 3, 3, 2], 4) == 254


def test_get_dropped_counts():
    assert get_dropped_counts(128, 4, 106) == 11
