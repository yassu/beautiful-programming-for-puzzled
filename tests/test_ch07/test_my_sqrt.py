import pytest

from beautiful_programming_for_puzzled.ch07.my_sqrt import my_sqrt


@pytest.mark.parametrize('x, a, b', [(2.0, 1.414, 1.415), (0.5, 0.707, 0.7071)])
def test_my_sqrt(x, a, b):
    res, counts = my_sqrt(x)
    assert abs(res**2 - x) < 1e-4
    assert counts >= 1


def test_my_sqrt_in_negative_case():
    with pytest.raises(ValueError, match='negative'):
        my_sqrt(-1.0)
