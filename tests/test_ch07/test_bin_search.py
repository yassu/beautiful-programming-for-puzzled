from random import choice, choices, shuffle

import pytest

from beautiful_programming_for_puzzled.ch07.bin_search import bin_search


@pytest.mark.parametrize('xs, target, expected', [
    ([1], 1, 0),
    ([], 1, -1),
])
def test_bin_search_in_clearly_case(xs, target, expected):
    assert bin_search(xs, target) == expected


@pytest.mark.repeat(100)
def test_bin_search():
    number = 10

    xs = choices(list(range(number)), k=number)
    xs.sort()
    target = choice(list(range(10)))

    res = bin_search(xs, target)

    if target not in xs:
        assert res == -1
    else:
        err_msg = f'input={xs}, res={res}'
        assert 0 <= res < number, err_msg
        assert xs[res] == target, err_msg
