from typing import Tuple

import pytest

from beautiful_programming_for_puzzled.ch11.fill_tile import (
    EMPTY_NUMBER,
    LOSS_NUMBER,
    Board,
    conv2str,
    get_quadrants,
)


@pytest.mark.parametrize(
    'number, expected', [
        (EMPTY_NUMBER, ' '),
        (LOSS_NUMBER, '!'),
        (0, '0'),
        (1, '1'),
    ])
def test_conv2str(number, expected):
    assert conv2str(number) == expected


def get_question(missing_pos: Tuple[int, int], size: int) -> Board:
    x, y = missing_pos
    board = [[EMPTY_NUMBER for j in range(size)] for i in range(size)]
    board[y][x] = LOSS_NUMBER
    return Board(board)


def test_Board_fill_in_case_2x2():
    board = get_question((1, 0), 2)
    assert board.fill() == Board([
        [0, LOSS_NUMBER],
        [0, 0],
    ])


def test_Board_fill_in_case_4x4():
    board = get_question((1, 1), 4)
    res = board.fill()
    m = LOSS_NUMBER
    a = res[0][0]
    b = res[0][2]
    c = res[1][2]
    d = res[2][0]
    e = res[2][3]

    assert res == Board([
        [a, a, b, b],
        [a, m, c, b],
        [d, c, c, e],
        [d, d, e, e],
    ])


def test_Board_humanly():
    board = Board(
        [
            [EMPTY_NUMBER, 0, LOSS_NUMBER],
            [EMPTY_NUMBER, 0, 0],
            [EMPTY_NUMBER, EMPTY_NUMBER, EMPTY_NUMBER],
        ])
    assert board.humanly == [[' ', '0', '!'], [' ', '0', '0'], [' ', ' ', ' ']]


def test_Board_fancy_repr():
    board = Board(
        [
            [EMPTY_NUMBER, 0, LOSS_NUMBER],
            [EMPTY_NUMBER, 0, 0],
            [EMPTY_NUMBER, EMPTY_NUMBER, EMPTY_NUMBER],
        ])
    assert board.fancy_repr == ' 0!\n 00\n   '


@pytest.mark.parametrize(
    'size, x, y, expected', [
        (8, 7, 7, 1),
        (8, 0, 7, 2),
        (8, 0, 0, 3),
        (8, 7, 0, 4),
        (8, 4, 4, 1),
        (8, 3, 4, 2),
        (8, 3, 3, 3),
        (8, 4, 3, 4),
    ])
def test_get_quadrants(size, x, y, expected):
    assert get_quadrants(size, x, y) == expected
