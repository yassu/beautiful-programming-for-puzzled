import pytest

from beautiful_programming_for_puzzled.ch08.invites import (
    DislikePair,
    Person,
    compute_invites_peoples,
    get_combinations,
    is_ok_combination,
)


def test_DislikePair_init():
    DislikePair(frozenset({Person('A'), Person('B')}))


def test_DislikePair_in_odd_size_case():
    with pytest.raises(ValueError, match='should be 2'):
        DislikePair(frozenset({Person('A'), Person('B'), Person('C')}))


def test_DislikePair_in_odd_type_case():
    with pytest.raises(ValueError, match='is not a Person instance'):
        DislikePair(frozenset({Person('A'), 3}))


def test_get_combinations():
    persons = [Person('A'), Person('B'), Person('C')]
    combinations = list(get_combinations(persons))

    assert combinations == [
        (Person('A'), Person('B'), Person('C')), (Person('A'), Person('B')),
        (Person('A'), Person('C')), (Person('B'), Person('C')), (Person('A'),), (Person('B'),),
        (Person('C'),), ()
    ]


@pytest.mark.parametrize(
    'persons, dislike_pairs, expected', [
        (
            [Person('A'), Person('B'),
             Person('C'), Person('D'),
             Person('E')], {
                 DislikePair(frozenset({Person('A'), Person('E')})),
                 DislikePair(frozenset({Person('A'), Person('F')})),
             }, False),
        (
            [Person('A'), Person('B'),
             Person('C'), Person('D'),
             Person('E')], {
                 DislikePair(frozenset({Person('F'), Person('G')})),
                 DislikePair(frozenset({Person('F'), Person('H')})),
             }, True),
    ])
def test_is_ok_combination(persons, dislike_pairs, expected):
    assert (is_ok_combination(persons, dislike_pairs) is expected)


def test_compute_invites_people():
    assert compute_invites_peoples([], {}) == tuple()
    assert compute_invites_peoples([Person('A')], {}) == (Person('A'),)
    persons = [Person(c) for c in 'ABCDEF']
    dislike_pairs = {
        DislikePair(frozenset({Person('A'), Person('F')})),
        DislikePair(frozenset({Person('A'), Person('E')})),
        DislikePair(frozenset({Person('A'), Person('B')})),
        DislikePair(frozenset({Person('E'), Person('F')})),
        DislikePair(frozenset({Person('B'), Person('C')})),
        DislikePair(frozenset({Person('C'), Person('D')})),
        DislikePair(frozenset({Person('D'), Person('E')})),
    }
    people = set(compute_invites_peoples(persons, dislike_pairs))
    assert people == {Person('B'), Person('D'), Person('F')}
