from beautiful_programming_for_puzzled.ch10.r_gcd import r_gcd


def test_r_gcd():
    assert r_gcd(2002, 1344) == 14
