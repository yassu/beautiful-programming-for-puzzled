from beautiful_programming_for_puzzled.ch10.r_fib import r_fib


def test_r_fib():
    assert r_fib(10) == 55
