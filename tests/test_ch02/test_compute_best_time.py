import pytest

from beautiful_programming_for_puzzled.ch02.compute_best_time import compute_best_time

SCHEDULE1 = [
    (6.0, 8.0), (6.0, 12.0), (6.0, 7.0), (7.0, 8.0), (7.0, 10.0), (8.0, 9.0), (8.0, 10.0),
    (9.0, 12.0), (9.0, 10.0), (10.0, 11.0), (10.0, 12.0), (11.0, 12.0)
]
EXPECTED1 = (5, 9.0)

SCHEDULE2 = [
    (6.0, 8.0), (6.5, 12), (6.5, 7.0), (7.0, 8.0), (7.5, 10.0), (8.0, 9.0), (8.0, 10.0),
    (9.0, 12.0), (9.5, 10.0), (10.0, 11.0), (10.0, 12.0), (11.0, 12.0)
]
EXPECTED2 = (5, 9.5)


@pytest.mark.parametrize('arg, expected', [(SCHEDULE1, EXPECTED1), (SCHEDULE2, EXPECTED2)])
def test_compute_best_time(arg, expected):
    assert compute_best_time(arg) == expected
